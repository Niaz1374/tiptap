import { Mark } from 'tiptap'
import { toggleMark, markInputRule, markPasteRule } from 'tiptap-commands'

export default class Rtl extends Mark {

  get name() {
    return 'rtl'
  }

  get schema() {
    return {
      parseDOM: [
        {
          tag: 'd',
        },
        {
          style: 'direction',
          getAttrs: value => value === 'rtl',
        },
      ],
      toDOM: () => ['d', 0],
    }
  }

  get defaultOptions() {
    return 'ltr'
  }

  keys({ type }) {
    return {
      'Mod-r': toggleMark(type),
    }
  }

  commands({ type }) {
    return () => toggleMark(type)
  }

  inputRules({ type }) {
    return [
      markInputRule(/~([^~]+)~/, type),
    ]
  }

  pasteRules({ type }) {
    return [
      markPasteRule(/~([^~]+)~/g, type),
    ]
  }

}
